﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour 
{
	public static GameManager instance;

	public static int gameProgress = 0;

	private static bool loadingFromGameOver;

	public bool gameOver;

	public float levelAudioVolume = 1f;

	private int health = 2;

	void Start()
	{
		instance = this;

		AudioController.Instance.SetLoopVolume (levelAudioVolume);

		if (loadingFromGameOver)
		{
			loadingFromGameOver = false;
			ScreenEffects.instance.screenFlash.Play ("gameover_fadein");
		} else
		{
			ScreenEffects.instance.screenFade.Play ("gameover_fadein");
		}
	}

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.R))
		{
			loadingFromGameOver = true;

			SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
		}
	}

	public void Victory()
	{
		if (!gameOver)
		{
			StartCoroutine (DoVictory ());
		}
	}

	IEnumerator DoVictory()
	{
		gameOver = true;
		if (Random.Range (0, 2) == 0)
		{
			AudioController.Instance.PlaySoundAtLimitedFrequency ("voice_victory", 0.25f);
		}
		BlobFace.instance.OnWin ();
		ScreenEffects.instance.screenFade.Play ("gameover");
		yield return new WaitForSeconds (4f);

		gameProgress++;
		SceneManager.LoadScene ("level_" + gameProgress);
	}

	public bool IsGameOver()
	{
		health--;
		HeartUI.instance.SetHealth (health);

		if (health <= 0)
		{
			StartCoroutine (DoGameOver ());
			return true;
		}

		return false;
	}

	IEnumerator DoGameOver()
	{
		gameOver = true;
		ScreenEffects.instance.GameOver ();
		yield return new WaitForSeconds (2f);
		loadingFromGameOver = true;

		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}
}
