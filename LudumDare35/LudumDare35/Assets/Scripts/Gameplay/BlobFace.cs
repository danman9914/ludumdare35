﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlobFace : MonoBehaviour {

	public static BlobFace instance;

	void Awake()
	{
		instance = this;
	}

	public void OnKill()
	{
		transform.parent.GetComponent<Animator> ().SetTrigger ("kill");
	}

	public void OnHit()
	{
		transform.parent.GetComponent<Animator> ().SetTrigger ("hit");
	}

	public void OnWin()
	{
		transform.parent.GetComponent<Animator> ().SetTrigger ("kill");
	}

	public void OnLose()
	{

	}
}
