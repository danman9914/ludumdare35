﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShapeController : MonoBehaviour {

	private List<Key> keys;

	void Start()
	{
		keys = new List<Key>(GetComponentsInChildren<Key> ());
	}

	void Update()
	{
		foreach (string keycode in Keys.KEYS)
		{
			CheckKeyStatus (keycode);
		}
	}

	private void CheckKeyStatus(string keycode)
	{
		if (Input.GetButtonDown (keycode))
		{
			OnKeyDown (keycode);
		}
		else if (Input.GetButtonUp (keycode))
		{
			//OnKeyUp (keycode);
		}
	}

	private void OnKeyDown(string keycode)
	{
		Key matchingKey = keys.Find (x => x.Code == keycode);

		if (matchingKey != null && AdjacentKeyIsActive (matchingKey))
		{
			Debug.Log ("ACTIVATE " + keycode);
			matchingKey.Activate ();
		} 
		else if (matchingKey == null)
		{
			Debug.Log ("NULL");
		}
	}

	private void OnKeyUp(string keycode)
	{
		Key matchingKey = keys.Find (x => x.Code == keycode);

		if (matchingKey != null)
		{
			matchingKey.Deactivate ();
		}
	}

	private bool AdjacentKeyIsActive(Key key)
	{
		if (KeyIsActive (GetKeyAtGrid (key.gridPosX - 1, key.gridPosY)))
		{
			return true;
		} else if (KeyIsActive (GetKeyAtGrid (key.gridPosX + 1, key.gridPosY)))
		{
			return true;
		} else if (KeyIsActive (GetKeyAtGrid (key.gridPosX, key.gridPosY - 1)))
		{
			return true;
		} else if (KeyIsActive (GetKeyAtGrid (key.gridPosX, key.gridPosY + 1)))
		{
			return true;
		} 
		else if (KeyIsActive (GetKeyAtGrid (key.gridPosX + 1, key.gridPosY + 1)))
		{
			return true;
		}
		else if (KeyIsActive (GetKeyAtGrid (key.gridPosX - 1, key.gridPosY + 1)))
		{
			return true;
		}
		else if (KeyIsActive (GetKeyAtGrid (key.gridPosX - 1, key.gridPosY - 1)))
		{
			return true;
		}
		else if (KeyIsActive (GetKeyAtGrid (key.gridPosX + 1, key.gridPosY - 1)))
		{
			return true;
		}else
		{
			Debug.Log ("CAN'T DO IT");
			return false;
		}
	}

	private bool KeyIsActive(Key key)
	{
		if (key == null)
		{
			return false;
		}

		return key.active;
	}

	private Key GetKeyAtGrid(int posx, int posy)
	{
		return keys.Find (x => x.gridPosX == posx && x.gridPosY == posy);
	}
}
