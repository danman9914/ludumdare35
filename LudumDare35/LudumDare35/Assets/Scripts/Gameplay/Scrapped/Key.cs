﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Key : MonoBehaviour 
{
	public string Code
	{
		get {
			return keyText.text.ToLower ();
		}
		set {
			keyText.text = value;
		}
	}

	public int gridPosX;
	public int gridPosY;

	public bool active;

	[SerializeField]
	private TextMesh keyText;

	[SerializeField]
	private Animator anim;

	void Start()
	{
		if (anim != null)
		{
			anim.gameObject.SetActive (false);
		}
	}

	public void Activate()
	{
		if (anim != null)
		{
			active = true;
			anim.gameObject.SetActive (true);
		}
	}

	public void Deactivate()
	{
		if (anim != null)
		{
			active = false;
			anim.gameObject.SetActive (false);
		}
	}
}
