﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Unit : MonoBehaviour 
{
	public enum Type 
	{
		MainBlob, 
		Blob,
		Idol,
		BasicEnemy,
		Any,
	};

	public GridSpace Tile
	{
		get {
			return currentTile;
		}
		set {
			if (currentTile != null)
			{
				currentTile.currentUnit = null;
			}
			currentTile = value;
			currentTile.currentUnit = this;
		}
	}

	[SerializeField]
	private GridSpace currentTile;

	public Type type;

	protected virtual void Start()
	{
		Tile = GridManager.instance.ClosestTile (transform.position);
	}
}
