﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyController : MonoBehaviour {

	public static EnemyController instance;

	[SerializeField]
	private List<EnemyUnit> enemies = new List<EnemyUnit>();

	public bool finalLevel;

	public bool enemyTurn;
	public bool showUI;

	void Awake()
	{
		instance = this;
		if (!finalLevel)
		{
			enemies = new List<EnemyUnit> (GetComponentsInChildren<EnemyUnit> ());
		}
	}

	public void KillEnemy(EnemyUnit enemy)
	{
		enemy.Die ();
		enemies.Remove (enemy);
	}

	public void EnemyTurn()
	{
		StartCoroutine (DoEnemyTurn ());
	}

	private IEnumerator DoEnemyTurn()
	{
		enemyTurn = true;
		yield return new WaitForSeconds (0.4f);
		showUI = true;
		yield return new WaitForEndOfFrame ();

		foreach (EnemyUnit e in enemies)
		{
			if (e != null)
			{
				yield return StartCoroutine (e.DoTurn ());
			}
		}

		if (enemies.Count == 0 && !finalLevel)
		{
			GameManager.instance.Victory ();
		}

		while (GameManager.instance.gameOver)
		{
			yield return new WaitForEndOfFrame ();
		}

		showUI = false;
		enemyTurn = false;
	}
}
