﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyUnit : Unit
{
	public Animator anim;
	public ParticleSystem blood;

	private const float REAL_MOVESPEED = 4f;

	protected override void Start()
	{
		base.Start ();

		//scuttle in
		transform.position = (Vector2)transform.position * 2.5f;
		StartCoroutine(DoMove (Tile));
	}

	public virtual IEnumerator DoTurn()
	{
		Unit adjacentBlob = GridManager.instance.TryGetAdjacentUnitOfType (Type.Blob, Tile);
		if (adjacentBlob != null)
		{
			yield return StartCoroutine (DoAttack ());
		} 
		else
		{
			yield return StartCoroutine (DoMove (ChooseTile ()));
		}
	}

	private IEnumerator DoAttack()
	{
		anim.SetTrigger ("attackfront");
		yield return new WaitForSeconds(0.35f);
	}

	private IEnumerator DoMove(GridSpace tile)
	{
		Tile = tile;
		if (Tile != null)
		{
			anim.SetBool ("moving", true);
			while (Vector2.Distance (transform.position, Tile.transform.position) > 0.1f)
			{
				transform.position = (Vector2)transform.position + (Vector2)(Tile.transform.position - transform.position).normalized * REAL_MOVESPEED * Time.deltaTime;
				yield return new WaitForEndOfFrame ();
			}
			anim.SetBool ("moving", false);
		}
		yield break;
	}

	public void Die()
	{
		AudioController.Instance.PlaySoundAtLimitedFrequency ("consume", 0.25f);
		AudioController.Instance.PlaySoundAtLimitedFrequency  ("voice_kill" + Random.Range(1,3),  0.25f);
		anim.SetTrigger ("die");
		ScreenEffects.instance.ZoomOnTarget (transform);
	}

	private void OnHit()
	{
		BlobFace.instance.OnHit ();
		ScreenEffects.instance.CameraShake ();
		AudioController.Instance.PlaySound ("hurt");
		AudioController.Instance.PlaySound ("voice_hurt");
		if (!GameManager.instance.IsGameOver ())
		{
			ScreenEffects.instance.Hit (transform.position);
		}
	}

	private void OnDieStrike()
	{
		blood.Play ();
		//ScreenEffects.instance.Hit (transform.position);
	}

	private void OnDieComplete()
	{
		Destroy (gameObject);
	}

	protected virtual GridSpace ChooseTile()
	{
		//move toward closest blob
		GridSpace left = GridManager.instance.OutOfBounds(Tile, Tile.x -1, Tile.y) ? null : GridManager.instance.grid [Tile.x - 1, Tile.y];
		GridSpace right = GridManager.instance.OutOfBounds(Tile, Tile.x +1, Tile.y) ? null : GridManager.instance.grid [Tile.x + 1, Tile.y];
		GridSpace up = GridManager.instance.OutOfBounds(Tile, Tile.x, Tile.y +1) ? null : GridManager.instance.grid [Tile.x, Tile.y + 1];
		GridSpace down = GridManager.instance.OutOfBounds(Tile, Tile.x, Tile.y -1) ? null : GridManager.instance.grid [Tile.x, Tile.y - 1];

		float leftDistance = left == null || (left.currentUnit && (left.currentUnit.type == Type.BasicEnemy || left.currentUnit.type == Type.Idol)) ? float.MaxValue : 
			Vector2.Distance(GridManager.instance.ClosestTile(left.transform.position, Type.Blob).transform.position, left.transform.position);
		
		float rightDistance = right == null || (right.currentUnit && (right.currentUnit.type == Type.BasicEnemy || right.currentUnit.type == Type.Idol)) ? float.MaxValue : 
			Vector2.Distance(GridManager.instance.ClosestTile(right.transform.position, Type.Blob).transform.position, right.transform.position);
		
		float upDistance = up == null || (up.currentUnit && (up.currentUnit.type == Type.BasicEnemy || up.currentUnit.type == Type.Idol)) ? float.MaxValue : 
			Vector2.Distance(GridManager.instance.ClosestTile(up.transform.position, Type.Blob).transform.position, up.transform.position);
		
		float downDistance = down == null || (down.currentUnit && (down.currentUnit.type == Type.BasicEnemy || down.currentUnit.type == Type.Idol)) ? float.MaxValue : 
			Vector2.Distance(GridManager.instance.ClosestTile(down.transform.position, Type.Blob).transform.position, down.transform.position);

		float minDist = Mathf.Min (leftDistance, Mathf.Min (rightDistance, Mathf.Min (upDistance, downDistance)));

		if (minDist == float.MaxValue)
		{
			return null;
		} else if (minDist == leftDistance)
		{
			return left;
		} else if (minDist == rightDistance)
		{
			return right;
		} else if (minDist == upDistance)
		{
			return up;
		} else //if (minDist == downDistance)
		{
			return down;
		}
	}
}
