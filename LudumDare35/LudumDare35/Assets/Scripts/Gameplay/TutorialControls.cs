﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TutorialControls : MonoBehaviour {

	public static TutorialControls instance;

	[SerializeField]
	SpriteRenderer c1;

	[SerializeField]
	SpriteRenderer c2;

	[SerializeField]
	private Color color1;

	[SerializeField]
	private Color color2;

	void Awake()
	{
		instance = this;
	}

	public void ShowControls()
	{
		c1.color = color2;
		c2.color = color2;
	}

	public void HideControls()
	{
		c1.color = color1;
		c2.color = color1;
	}
}
