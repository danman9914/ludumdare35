﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GridManager : MonoBehaviour 
{
	public static GridManager instance;

	public int w = 15;
	public int h = 9;

	public static int WIDTH = 15;
	public static int HEIGHT = 9;

	public GridSpace[,] grid;

	void Awake()
	{
		instance = this;
		WIDTH = w;
		HEIGHT = h;
		grid = new GridSpace[WIDTH, HEIGHT];
		BuildGrid ();
	}

	public Unit GetAdjacent(GridSpace space, Vector2 direction)
	{
		if (!OutOfBounds (space, direction))
		{
			return grid [space.x + (int)direction.x, space.y + (int)direction.y].currentUnit;
		}
		return null;
	}

	public bool OutOfBounds(GridSpace space, int newX, int newY)
	{
		if (newX < 0 || newX >= WIDTH || newY < 0 || newY >= HEIGHT)
		{
			return true;
		}

		return false;
	}

	public bool OutOfBounds(GridSpace space, Vector2 direction)
	{
		int newX = space.x + (int)direction.x;
		int newY = space.y + (int)direction.y;
		return OutOfBounds (space, newX, newY);
	}

	public Unit TryGetAdjacentUnitOfType(Unit.Type type, GridSpace space)
	{
		List<Unit> units = TryGetAllAdjacentUnitsOfType (type, space);
		if (units.Count > 0)
		{
			return units [0];
		}
		return null;
	}

	public List<Unit> TryGetAllAdjacentUnitsOfType(Unit.Type type, GridSpace space)
	{
		List<Unit> units = new List<Unit> ();

		Unit left = GetAdjacent (space, new Vector2 (-1f, 0f));
		Unit right = GetAdjacent (space, new Vector2 (1f, 0f));
		Unit up = GetAdjacent (space, new Vector2 (0f, 1f));
		Unit down = GetAdjacent (space, new Vector2 (0f, -1f));

		if (left != null && left.type == type)
		{
			units.Add(left);
		}
		if (right != null && right.type == type)
		{
			units.Add(right);
		}
		if (up != null && up.type == type)
		{
			units.Add(up);
		}
		if (down != null && down.type == type)
		{
			units.Add(down);
		}

		return units;
	}

	public GridSpace ClosestTile(Vector2 position, Unit.Type type = Unit.Type.Any)
	{
		GridSpace closestSpace = grid [0, 0];

		for (int x = 0; x < WIDTH; x++)
		{
			for (int y = 0; y < HEIGHT; y++)
			{
				if (type != Unit.Type.Any)
				{
					if (grid [x,y].currentUnit == null || grid[x,y].currentUnit.type != type)
					{
						continue;
					}
				}
				if (Vector2.Distance (grid [x, y].transform.position, position) < (Vector2.Distance (closestSpace.transform.position, position)))
				{
					closestSpace = grid [x, y];
				}
			}
		}

		return closestSpace;
	}

	private void BuildGrid()
	{
		var grid = this;

		//clear out old grid
		foreach (GridSpace child in grid.GetComponentsInChildren<GridSpace>())
		{
			if (child != grid.transform)
			{
				DestroyImmediate (child.gameObject);
			}
		}

		//make new grid
		for (int x = 0; x < GridManager.WIDTH; x++)
		{
			for (int y = 0; y < GridManager.HEIGHT; y++)
			{
				GameObject gridSpaceObj = Instantiate (Resources.Load<GameObject> ("Prefabs/GridSpace"));
				gridSpaceObj.transform.parent = grid.transform;
				gridSpaceObj.transform.position = (Vector2)grid.transform.position + new Vector2 (-(float)GridManager.WIDTH /2f +0.5f, -(float)GridManager.HEIGHT /2f +0.5f) + new Vector2 (x, y);
				gridSpaceObj.name = "TILE: " + x + "," + y;

				GridSpace space = gridSpaceObj.GetComponent<GridSpace> ();
				space.x = x;
				space.y = y;

				grid.grid [x, y] = space;
			}
		}
	}
}
