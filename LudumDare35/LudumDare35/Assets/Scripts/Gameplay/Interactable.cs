﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Collider2D))]
public class Interactable : MonoBehaviour {
	
	protected bool hovering = false;

	[Header ("Generic Interactable")]
	[SerializeField]
	private string clickSoundId;

	[SerializeField]
	private string hoverSoundId;

	protected virtual void Update() {
		
		Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		if (GetComponent<Collider2D> ().OverlapPoint (mousePos)) {

			if (!hovering) {
				OnStartHover();
			}

			if (Input.GetMouseButtonDown (0)) {
				OnClick ();
			}

			hovering = true;
		} else {
			if (hovering) {
				OnStopHover();
			}

			hovering = false;
		}
	}

	protected virtual void OnClick() { }

	protected virtual void OnStartHover() { }

	protected virtual void OnStopHover() { }
}
