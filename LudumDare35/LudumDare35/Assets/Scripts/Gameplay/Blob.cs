﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Unit))]
public class Blob : Interactable
{
	public Animation anim;

	public Unit Unit
	{
		get {
			return GetComponent<Unit> ();
		}
	}

	private ArrowUI arrows;

	protected override void Update()
	{
		base.Update ();

		if (!EnemyController.instance.enemyTurn)
		{
			if (hovering)
			{
				if (Input.GetButtonDown ("up"))
				{
					SpawnBlob (new Vector2 (0f, 1f));
				} else if (Input.GetButtonDown ("down"))
				{
					SpawnBlob (new Vector2 (0f, -1f));
				} else if (Input.GetButtonDown ("left"))
				{
					SpawnBlob (new Vector2 (-1f, 0f));
				} else if (Input.GetButtonDown ("right"))
				{
					SpawnBlob (new Vector2 (1f, 0f));
				}
			}
		}

		if (arrows != null)
		{
			arrows.gameObject.SetActive (!EnemyController.instance.enemyTurn);
		}
	}

	protected override void OnStartHover()
	{
		if (!EnemyController.instance.enemyTurn)
		{
			AudioController.Instance.PlaySound ("select");
		}
		GameObject arrowUI = Instantiate (Resources.Load<GameObject> ("Prefabs/ArrowUI"));
		arrowUI.transform.position = transform.position;
		arrows = arrowUI.GetComponent<ArrowUI> ();

		if (TutorialControls.instance)
		{
			TutorialControls.instance.ShowControls ();
		}
	}

	protected override void OnStopHover()
	{
		arrows.Hide ();

		if (TutorialControls.instance)
		{
			TutorialControls.instance.HideControls ();
		}
	}

	public void SpawnBlob(Vector2 direction, bool endTurn = true)
	{
		if (!GridManager.instance.OutOfBounds (Unit.Tile, direction))
		{
			Unit adjacent = GridManager.instance.GetAdjacent (Unit.Tile, direction);
			if (adjacent != null && adjacent.type == Unit.Type.BasicEnemy)
			{
				StartCoroutine(DoSpawnIntoEnemy (adjacent));
			}
			if (adjacent == null || adjacent.type != Unit.Type.Blob)
			{
				GameObject newBlob = Instantiate (Resources.Load<GameObject> ("Prefabs/Blob"));
				newBlob.transform.parent = transform;
				newBlob.transform.position = (Vector2)transform.position + direction;

				newBlob.GetComponent<Unit> ().Tile = GridManager.instance.grid [GetComponent<Unit> ().Tile.x + (int)direction.x, GetComponent<Unit> ().Tile.y + (int)direction.y];

				float newRot = 90f;
				if (direction.x == 1f)
				{
					newRot = 180f;
				} else if (direction.x == -1f)
				{
					newRot = 0f;
				} else if (direction.y == 1f)
				{
					newRot = -90f;	
				}
				newBlob.transform.rotation = Quaternion.Euler (0f, 0f, newRot);
				newBlob.GetComponent<Blob> ().anim.Play ();

				if (adjacent != null && adjacent.type == Unit.Type.Idol)
				{
					StartCoroutine(DoSpawnIntoIdol (adjacent));
				}

				if (endTurn)
				{
					EnemyController.instance.EnemyTurn ();
				}
			} else
			{
				OnIllegalSpawn ();
			}
		} else
		{
			OnIllegalSpawn ();
		}
	}

	private IEnumerator DoSpawnIntoEmpty()
	{
		yield break;
	}

	private IEnumerator DoSpawnIntoIdol(Unit idolUnit)
	{
		foreach (Blob blob in GameObject.FindObjectsOfType<Blob>())
		{
			List<Unit> enemies = GridManager.instance.TryGetAllAdjacentUnitsOfType (Unit.Type.BasicEnemy, blob.Unit.Tile);
			foreach (Unit enemy in enemies)
			{
				Vector2 dir = new Vector2 (enemy.Tile.x - blob.Unit.Tile.x, enemy.Tile.y - blob.Unit.Tile.y);
				blob.SpawnBlob (dir, endTurn:false);
			}
		}
		Destroy (idolUnit.transform.parent.gameObject);
		yield break;
	}

	private IEnumerator DoSpawnIntoEnemy(Unit enemyUnit)
	{
		EnemyController.instance.KillEnemy((EnemyUnit)enemyUnit);
		BlobFace.instance.OnKill ();
		yield break;
	}

	private void OnIllegalSpawn()
	{
		Debug.Log ("NOPE");
	}
}
