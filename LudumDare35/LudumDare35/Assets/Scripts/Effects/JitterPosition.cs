﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JitterPosition : MonoBehaviour {
	private Vector2 currentJitterValue;
	private Vector2 intendedJitterValue;

	private float jitterTimer;

	private const float JITTER_SPEED = 5f;
	private const float JITTER_FREQUENCY = 0.1f;

	private Vector2 originalScale;
	private Vector2 originalPos;

	public float amount = 0.05f;

	void Start()
	{
		originalScale = transform.localScale;
		originalPos = transform.localPosition;
	}

	void Update()
	{
		jitterTimer += Time.deltaTime;
		if (jitterTimer > JITTER_FREQUENCY)
		{
			SetNewIntendedValue();
			jitterTimer = 0f;
		}

		currentJitterValue = Vector2.Lerp(currentJitterValue, intendedJitterValue, Time.deltaTime * JITTER_SPEED);
		ApplyJitter(currentJitterValue);
	}

	private void SetNewIntendedValue()
	{
		intendedJitterValue = Random.insideUnitCircle;
	}

	private void ApplyJitter(Vector2 jitterValue) 
	{
		//transform.localScale = new Vector2 (originalScale.x + jitterValue.x * 0.3f, originalScale.y + jitterValue.y * 0.3f);
		transform.localPosition = new Vector2 (originalPos.x + jitterValue.x * amount, originalPos.y + jitterValue.y *amount); 
	}
}
