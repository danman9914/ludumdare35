﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScreenEffects : MonoBehaviour {

	public static ScreenEffects instance;

	public Animation screenFlash;
	public Animation screenFade;

	float SCALE_SPEED = 15.0f;
	float MOVE_SPEED = 15.0f;

	public float defaultSize = 4.5f;
	[HideInInspector]
	public float intendedSize = 4.5f;
	public Vector2 intendedPosition = new Vector2(0f,0f);
	Transform intendedTransform;

	public bool finalLevel;
	public float maxY;
	public float minY;

	void Awake () {
		instance = this;
		intendedSize = defaultSize;
	}

	public void Hit(Vector2 pos) {
		StartCoroutine(HitAnim(pos));
	}

	IEnumerator HitAnim(Vector2 pos) {
		yield return new WaitForSeconds (0.1f);
		screenFlash.Stop ();
		screenFlash.Play();
		yield return new WaitForSeconds(0.025f);
		Time.timeScale = 0.1f;
		yield return new WaitForSeconds(0.0025f);
		Time.timeScale = 1;
	}

	public void ZoomOnTarget(Transform target) {
		intendedTransform = target;
		intendedSize = defaultSize * 0.9f;

		StartCoroutine(SlowZoom());
	}

	IEnumerator SlowZoom() {
		yield return new WaitForSeconds( 0.15f);
		Time.timeScale = 0.1f;
		yield return new WaitForSeconds(0.025f);
		Time.timeScale = 1;
		ResetZoom();
	}

	public void CameraShake()
	{
		StopCoroutine (DoCamShake ());
		StartCoroutine (DoCamShake ());
	}

	IEnumerator DoCamShake()
	{
		intendedPosition = (Vector2)transform.position + Random.insideUnitCircle * 0.1f;
		yield return new WaitForSeconds( 0.05f);
		intendedPosition = (Vector2)transform.position + Random.insideUnitCircle * 0.1f;
		yield return new WaitForSeconds( 0.05f);
		intendedPosition = (Vector2)transform.position + Random.insideUnitCircle * 0.1f;
		yield return new WaitForSeconds( 0.05f);
		intendedPosition = Vector2.zero;
	}

	public void ResetZoom() {
		intendedTransform = null;
		intendedPosition = Vector2.zero;
		intendedSize = defaultSize;
	}

	public void GameOver()
	{
		screenFlash.Stop ();
		screenFlash.Play ("gameover");
	}

	void Update () {

		if (intendedTransform) {
			intendedPosition = intendedTransform.position * 0.25f;
		}

		//lerp size
		float size = Mathf.Lerp(GetComponent<Camera>().orthographicSize, intendedSize, Time.deltaTime * SCALE_SPEED);
		GetComponent<Camera>().orthographicSize = size;

		if (finalLevel)
		{
			Blob[] blobs = GameObject.FindObjectsOfType<Blob> ();
			Blob lowestBlob = blobs [0];
			foreach (Blob b in blobs)
			{
				if (b.transform.position.y < lowestBlob.transform.position.y)
				{
					lowestBlob = b;
				}
			}
			intendedPosition = new Vector2 (0f, Mathf.Clamp(lowestBlob.transform.position.y, minY, maxY));

			if (lowestBlob.transform.position.y < -22f)
			{
				GameManager.instance.Victory ();
			}
		}

		//lerp position
		Vector2 position = Vector2.Lerp((Vector2)transform.position, intendedPosition, Time.deltaTime * MOVE_SPEED);
		transform.position = new Vector3(position.x, position.y, transform.position.z);

	}
}
