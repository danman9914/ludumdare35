﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlickeringLight : MonoBehaviour {

    private float currentFlickerValue;
    private float intendedFlickerValue;

    private float flickerTimer;

    private readonly RangeAttribute FLICKER_RANGE = new RangeAttribute(-1f, 1f);
    private const float FLICKER_SPEED = 4f;
    private const float FLICKER_FREQUENCY = 0.15f;

	[SerializeField]
	private Light light3d;

    void Update()
    {
        flickerTimer += Time.deltaTime;
        if (flickerTimer > FLICKER_FREQUENCY)
        {
            SetNewIntendedValue();
            flickerTimer = 0f;
        }

        currentFlickerValue = Mathf.Lerp(currentFlickerValue, intendedFlickerValue, Time.deltaTime * FLICKER_SPEED);
        ApplyFlicker(currentFlickerValue);
    }

    private void SetNewIntendedValue()
    {
        intendedFlickerValue = FLICKER_RANGE.min + Random.value * Mathf.Abs(FLICKER_RANGE.max - FLICKER_RANGE.min);
    }

	private void ApplyFlicker(float flickerValue) 
	{
		light3d.spotAngle = flickerValue * 15f + 150f;
	}
}
