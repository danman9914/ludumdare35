﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Keys : MonoBehaviour {
	
	public static readonly List<string> KEYS = new List<string> () {
		"left shift",
		"q",
		"w",
		"e",
		"r",
		"t",
		"y",
		"u",
		"i",
		"o",
		"p",
		"[",
		"]",
		"a",
		"s",
		"d",
		"f",
		"g",
		"h",
		"j",
		"k",
		"l",
		";",
		"'",
		"z",
		"x",
		"c",
		"v",
		"b",
		"n",
		"m",
		",",
		".",
		"/",
	};
}
