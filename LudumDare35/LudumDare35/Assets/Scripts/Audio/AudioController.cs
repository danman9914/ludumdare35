using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class AudioController : MonoBehaviour {
	
	List<AudioClip> SFX = new List<AudioClip>();
	List<AudioClip> Loops = new List<AudioClip>();
	public static AudioController Instance {get; private set;}
	AudioSource source;
	
	public string InitialSong;

	private string currentSong = "";

	private Dictionary<string, float> limitedFrequencySounds = new Dictionary<string, float>();
	private Dictionary<string, int> lastPlayedSounds = new Dictionary<string, int> ();
		
	void Awake() {
		DontDestroyOnLoad (gameObject);
		if (Instance) {
			Destroy(gameObject);
			return;
		}
		Instance = this;

		source = GetComponent<AudioSource>();

		foreach (object o in Resources.LoadAll("Audio/SFX")) {
			SFX.Add((AudioClip)o);
		}
		foreach (object o in Resources.LoadAll("Audio/BGM")) {
			Loops.Add((AudioClip)o);
		}

		CrossFadeLoop(InitialSong, 1f);
	}
	
	AudioSource PlayClipAt (AudioClip clip, Vector3 pos) {
		GameObject tempGO = new GameObject("TempAudio");
		tempGO.transform.position = pos;
		AudioSource aSource = tempGO.AddComponent<AudioSource>();
		aSource.clip = clip;
		aSource.Play();
		Destroy(tempGO, clip.length);
		return aSource;
	}

	public AudioSource PlaySoundAtLimitedFrequency (string soundName, float frequencyMin, float pitch = 1f, float volume = 1f) {

		if (limitedFrequencySounds.ContainsKey(soundName)) {
			if (Time.time - frequencyMin > limitedFrequencySounds[soundName]) {
				limitedFrequencySounds[soundName] = Time.time;
				return PlaySoundWithPitch (soundName, pitch, volume);
			}
		}
		else {
			limitedFrequencySounds.Add(soundName, Time.time);
			return PlaySoundWithPitch (soundName, pitch, volume);
		}

		return null;
	}
	
	public AudioSource PlaySound(string soundName) 
	{
		AudioClip wav = SFX.Find (x => x.name == soundName);

		if (wav != null)
		{
			return PlayClipAt(wav, Vector3.zero);
		}

		return null;
	}

	public AudioSource PlayRandomSound(string soundPrefix, bool noRepeating = false, bool randomPitch = false)
	{
		List<AudioClip> variations = SFX.FindAll (x => x.name.Contains (soundPrefix));

		int index = Random.Range (0, variations.Count) + 1;
		if (noRepeating)
		{
			if (!lastPlayedSounds.ContainsKey(soundPrefix))
			{
				lastPlayedSounds.Add (soundPrefix, index);
			}
			else 
			{
				while (lastPlayedSounds[soundPrefix] == index)
				{
					index = Random.Range (0, variations.Count) + 1;
				}
				lastPlayedSounds [soundPrefix] = index;
			}
		}

		string soundId = soundPrefix + " " + index;
		return PlaySoundWithPitch (soundId, randomPitch ? Random.Range(0.9f, 1.1f) : 1f, 1f);
	}

	public void PlaySoundDelayed(string soundName, float delay, float pitch = 1f, float volume = 1f) {
		StartCoroutine (WaitThenPlaySound (soundName, delay, pitch, volume));
	}

	IEnumerator WaitThenPlaySound(string soundName, float delay, float pitch, float volume) {
		yield return new WaitForSeconds (delay);
		PlaySoundWithPitch (soundName, pitch, volume);
	}
	
	public AudioSource PlaySoundWithPitch(string soundName, float pitch, float volume) {
		if (!string.IsNullOrEmpty (soundName)) {
			AudioSource a = PlaySound (soundName);
			a.pitch = pitch;
			a.volume = volume;
			return a;
		} else {
			return null;
		}
	}

	public void PauseLoop() {
		source.Pause ();
	}

	public void ResumeLoop() {
		source.Play ();
	}

	public void StopLoop() {
		source.Stop();
	}

	public void SetLoopVolume(float volume) {
		GetComponent<AudioSource> ().volume = volume;
	}

	public void FadeLoopIfNotPlaying(string loopName, float volume) {
		if (loopName != currentSong)
			CrossFadeLoop (loopName, volume);
	}

	public void CrossFadeLoopIfNotPlaying(string loopName, float volume, float speed = 0.5f, float pitch = 1f) {
		if (currentSong != loopName) {
			CrossFadeLoop(loopName, volume, speed, pitch);
		}
	}
	
	public void CrossFadeLoop(string loopName, float volume, float speed = 0.5f, float pitch = 1f) {
		currentSong = loopName;
		for (int i = 0; i < Loops.Count; i ++) {
			AudioClip wav = Loops[i];
			if (wav.name == loopName) {
				//StopCoroutine("Cross");
				StopAllCoroutines();
				StartCoroutine(Cross(wav,volume,false, speed, pitch));
				return;
			}
		}
	}
	
	public void FadeOutLoop() {
		StartCoroutine(Cross(null,0,true));
	}

	public void FadeOutThenPlayAfterDelay(string bgm, float delay) {
		FadeOutLoop();
		StartCoroutine(WaitThenPlay(bgm, delay));
	}

	IEnumerator WaitThenPlay(string bgm, float waitTime) {
		yield return new WaitForSeconds(waitTime);
		CrossFadeLoop(bgm, 1f);
	}
	
	IEnumerator Cross (AudioClip newLoop, float volume, bool fadeOut, float speed = 0.5f, float pitch = 1f) {
		//If a loop is playing, fade it out
		if (source.clip) {
			while (source.volume > 0 + 0.2f) {
				source.volume = Mathf.Max (0, source.volume - speed);
				yield return new WaitForEndOfFrame();
			}
		}
		
		if (!fadeOut) {
			//Fade in the new loop to be played
			source.clip = newLoop;
			source.volume = 0;
			source.pitch = pitch;
			source.Play();
			while (source.volume < volume - 0.2f) {
				source.volume = Mathf.Min (volume, source.volume + 0.5f);
				yield return new WaitForEndOfFrame();
			}
		}
	}
}
