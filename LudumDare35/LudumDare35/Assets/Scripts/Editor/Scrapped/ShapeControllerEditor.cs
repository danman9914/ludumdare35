﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(ShapeController))]
public class ShapeControllerEditor : Editor {

	public override void OnInspectorGUI()
	{
		EditorGUILayout.LabelField ("");
		EditorGUILayout.LabelField ("BUILD KEYS", EditorStyles.boldLabel);
		if (GUILayout.Button ("Build"))
		{
			BuildKeys ();
		}

		EditorUtility.SetDirty (target);
	}

	private const float SPACING = 0.85f;
	private const float ROW_OFFSET = 0.25f;

	private void BuildKeys()
	{
		var shape = target as ShapeController;

		//clear out old keys
		foreach (Key key in shape.transform.GetComponentsInChildren<Key>())
		{
			if (key.Code != "left shift")
			{
				DestroyImmediate (key.gameObject);
			}
		}

		//build keys
		int x = 0;
		int y = 0;
		foreach (string code in Keys.KEYS)
		{
			if (code == "left shift")
			{
				continue;
			}
			
			GameObject keyObj = Instantiate (Resources.Load<GameObject> ("Prefabs/Key"));
			keyObj.name = "Key " + code.ToUpper ();
			keyObj.transform.parent = shape.transform;
			keyObj.transform.position = (Vector2)shape.transform.position + new Vector2 (x * SPACING + y * ROW_OFFSET, y * -SPACING);

			Key key = keyObj.GetComponent<Key> ();
			key.Code = code;
			key.gridPosX = x;
			key.gridPosY = y;

			x++;
			if (code == "]" || code == "'")
			{
				x = 0;
				y++;
			}
		}
	}
}
