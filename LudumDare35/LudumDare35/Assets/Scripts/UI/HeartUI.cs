﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HeartUI : MonoBehaviour 
{
	[SerializeField]
	private Animation h1;

	[SerializeField]
	private Animation h2;

	public static HeartUI instance;

	void Awake()
	{
		instance = this;
	}

	public void SetHealth(int h)
	{
		if (h == 1)
		{
			if (h1 != null)
			{
				h1.Play ();
				Destroy (h1.gameObject, 0.3f);
			}
		}
		if (h == 0)
		{
			if (h2 != null)
			{
				h2.Play ();
				Destroy (h2.gameObject, 0.3f);
			}
		}
	}
}
