﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyTurnIndicator : MonoBehaviour {

	[SerializeField]
	private GameObject turnUI;

	void Update()
	{
		turnUI.SetActive (EnemyController.instance.showUI && !GameManager.instance.gameOver);	
	}
}
