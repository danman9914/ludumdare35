﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class StartScreen : MonoBehaviour 
{
	[SerializeField]
	private Animation anim;

	void Update()
	{
		if (Input.GetMouseButtonDown (0))
		{
			this.enabled = false;
			StartCoroutine (StartGame ());
		}
	}

	IEnumerator StartGame()
	{
		anim.Play ("gameover");
		AudioController.Instance.PlaySound ("voice_victory");
		yield return new WaitForSeconds(4f);
		SceneManager.LoadScene ("level_0");
	}
}
