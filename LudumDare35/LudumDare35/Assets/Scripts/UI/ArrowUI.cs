﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ArrowUI : MonoBehaviour {


	public static ArrowUI instance;

	[SerializeField]
	private Animation anim;

	public void Hide()
	{
		anim.Play ("arrow_ui_close");
		Destroy (gameObject, 0.25f);
	}
}
