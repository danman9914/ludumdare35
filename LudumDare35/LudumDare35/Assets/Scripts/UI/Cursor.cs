﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Cursor : MonoBehaviour
{
	public static Cursor instance;

	[SerializeField]
	private Camera screenCamera;

	private Vector2 intendedPosition;
	private float moveSpeed = 30f;

	void Awake()
	{
		instance = this;
	}

	void Update()
	{
		UnityEngine.Cursor.visible = false;
#if UNITY_EDITOR
		UnityEngine.Cursor.visible = Input.GetKey(KeyCode.Q);
#endif
		intendedPosition = screenCamera.ScreenToWorldPoint (Input.mousePosition);

		transform.position = Vector2.Lerp (transform.position, intendedPosition, Time.deltaTime * moveSpeed);
	}
}