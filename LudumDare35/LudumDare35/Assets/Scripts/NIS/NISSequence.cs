﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class NISSequence : MonoBehaviour 
{

	private void PlaySound(string sound)
	{
		AudioController.Instance.PlaySound ("voice_kill2");
	}

	private void OnChangeScenes()
	{
		GameManager.gameProgress++;
		SceneManager.LoadScene ("level_" + GameManager.gameProgress);
	}
}
